# Kromosome_nav.sh  

This program provides memory of traversed directories enabling the user to "backtrack" to a previously visited directory by displaying the previously visited paths and enabling the user to select the required path number from the displayed list to quickly return the current working directory to that path with minimal key presses. This feature, although currently provided for by the Linux history command, it does not allow cross shell session access to the command history. 

Kromosome_nav.sh provides bash cross-session access to the ls & cd command history regardless of the logged in user by virtue of persisting all ls_ and cd_ commands executed by the user in the Bash terminal, the user may then recall history of either command to display it, with the option to enter an integer corresponding to the required command for immediate execution, thereby minimising the amount of keystrokes to achieve one's goal.

**Note:** *Unlike the Linux history command, the cd_hist and ls_hist commands do not, by default, persist their histories between system restarts. Should you wish to enable this feature, simply edit kromosome_nav.sh and enter new directory location values for the LS_HIST_FILE & CD_HIST_FILE variables other than the /tmp directory. Be sure to source the kromosome_nav.sh file in the immediate terminal window to test your saved edits.*
 
The various commands provided by thie "shell extension" are:  
																																						
### ls_  
ls_ works exactly the same as ls, except it has provision for remembering the paths and provides quick and easy recall and re-execution irrespective of shell session. The history is persisted to a temp file so this is accessible from any shell session.

### ls_hist  
ls_hist_ provides the historical listing of the user's usage of the ls_ command. The ls_ history file has the default path and name of: /tmp/.lshist.


### cd_
cd_ works exactly the same as cd, except it has provision for remembering the paths and provides quick and easy recall and re-execution irrespective of shell session. The history is persisted to a temp file so this is accessible from any shell session.  

### cd_hist
cd_hist provides the historical listing of the user's usage of the cd_ command. The cd_ history file has the default path and name of: /tmp/.cdhist.  

## How to enable  

In Linux like systems, save the kromosome_nav.sh file somewhere on your system. To ensure that the file is executable by your user, execute the following in your bash shell to, (1) change wnership to your logged on user, and (2) add the executable flag to the file:  

```
  $ sudo chown $USER:USER kromosome_nav.sh
  $ sudo chmod u+x kromosome_nav.sh
```

Once you have setup the file ownerships and executable flag on the file, you now need to ensure that the commands are made available to all of your bash shell sessions. To do this, edit your ~/.bash_aliases file to include the following:

```
  source /directory/location/kromosome.sh
```

Alternatively you can achieve the same by executing the following in your terminal:  

```
  $ echo "# provision for ls_ & cd_ commands
    source /directory/location/kromosome_nav.sh" >> ~/.bash_aliases  
```

## How to use  

Essentially every possible use of the ls command is mirrored with the ls_ command. To get assistance with the syntax you can view the help manual for ls by typing man ls into your terminal.  

Examples of use for the ls_ command follow:  


```
  $ ls_
  $ ls_ .
  $ ls_ -lah
  $ ls_ -lah ~/Documents
```  

Examples of use for the cd_ command follow:

```
  $ cd_ ~/Documents
	$ cd_ ../
``` 
Example of the ls_hist command  

```
  $ ls_hist
    1...-lah ~/Documents
    2.../var/www/html
    3.../var/www
    4...-lah /var/www
		Enter choice or (q) to quit:
```

Example of the cd_hist command  

``` 
	$ cd_hist
    1...~/Documents/
		2...~/Development/project/
		3.../home/user/
    4.../var/www/html
		5.../var/www/html/project
		Enter choice or (q) to quit:
```  

## Author  

Mathew Garland  
Kromosome Industries  
email: matg74@gmail.com  

## MIT License

Copyright <2018> <Kromosome Industries - Mathew Garland [matg74@gmail.com]>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
