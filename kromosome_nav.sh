#! /bin/bash
#*****************************************************************************#
# Copyright <2018> <Kromosome Industries - Mathew Garland>										#
# Author: matg74@gmail.com            																				#
# License: MIT																																#
#*****************************************************************************#
# This program provides memory of traversed directories enabling the user to 	#
# "backtrack" to a previously visited directory by displaying the previously 	#
# visited paths and enabling the user to select the required path number from #
# the displayed list quickly return the cwd to that path with minimal key     #
# presses.	                                                                  #
#																																		          #
# The commands provided are:																							    #
#																																							#
# ls_ which works exactly the same as ls, except it has provision for 				#
# remembering the paths and provides quick and easy recall and re-execution.	#
#																																							#
# ls_hist which provides the historical listing of the user's usage of the ls_#
# command.																																		#
#																																							#
# cd_ which works exactly the same as cd, except it has provision for 				#
# remembering the paths and provides quick and easy recall and re-execution.	#
#																																							#
# cd_hist which provides the historical listing of the user's usage of the ls_#
# command.																																		#
#  																																						#
# Minimum Requirements:																												#
# - Bash Version 4 +                                                   				#
#																																							#
# TODO: Implement options to enable the user to sort and group history listing#
# so each directory command displays a number indicating the amount of times  #
# the command has been executed.																							#
#*****************************************************************************#
# Global Var's
LS_HIST_FILE="/tmp/.lshist"			# local history file for ls_ commands
CD_HIST_FILE="/tmp/.cdhist"			# local history file for cd_ commands
# The ls_ function which will provide a history of paths queried by the user
# for easy re-execution when displaying the history using the function ls_hist
# independent of shell sessions.
function ls_() {
  # local Var's
  declare -i ARG_NUM=("${#}")		# number of arguments supplied to ls_ func
  declare -A MAP        				# map data to hold function arguments
  CWD=''												# current working directory
  OPT=''												# command line options
  OUTPUT=''											# output to be displayed
  # populate the map with function args
	for args in "$@" 
	do 
		MAP["${args}"]="$args" 
	done
  # if no function arguments supplied the function has been called with only "ls_"
  if [[ "${ARG_NUM}" -eq 0 ]]; then
    CWD="${PWD}"
	# function called with 1 or more args
	else
    # regex to test for any ls command line parameters, i.e. -alh, OR --help
    OPTIONS_REGEX="(^\-|^\--)\w+$"
    # when one or more args exist, iterate them
    for c in "${!MAP[@]}"; do
      # if there is a period in the text, expand pwd into CWD		
		  if [ "$c" = "." ]; then
        CWD="${PWD}"
			# check to see if ls command options -/-- were passed
      elif [[ "${c}" =~ $OPTIONS_REGEX ]]; then
        OPT="$c"
				# if the only supplied argument is options, set the path to CWD 
				if [[ "${ARG_NUM}" -eq 1 ]]; then
				  CWD="${PWD}"
				fi
      else
				# the argument is the path
				CWD="${c}"
      fi 
    done
  fi
	# check for ls command options, these must be printed first before any path
  if [[ ! -z "${OPT// }" ]]; then
    OUTPUT+="${OPT}"
  fi
	# check for empty vars ( true if not equal to zero length )
  if [[ ! -z "${CWD}" ]]; then
    # if we already populated the output with the options, add spaces
    if [[ ! -z "${OPT// }" ]]; then
      OUTPUT+=" ${CWD}"
    else
			# do not add spaces
      OUTPUT+="${CWD}"
    fi
  fi
	# if the directory exists proceed
	if [ -d "${CWD}" ]; then
		# save the output to the history file
  	(echo $OUTPUT >> $LS_HIST_FILE)
  	# execute the command in a sub-shell
  	ls $OUTPUT
	else
		echo "${CWD} is not a valid path."
	fi
}
# The cd_ function which will provide a history of directory changes by the user
# for easy re-execution when displaying the history using the function cd_hist
# independent of shell sessions.
function cd_() {
  # local Var's
  declare -i ARG_NUM=("${#}")		# number of arguments supplied to ls_ func
  declare -A MAP        				# map data to hold function arguments
	declare -i err=0							# error flag
  CWD=''												# current working directory
  OPT=''												# command line options
  OUTPUT=''											# output to be displayed
  # populate the map with function args
	for args in "$@" 
	do 
		MAP["${args}"]="$args" 
	done
  # if no function arguments supplied the function has been called with only "ls_"
  if [[ "${ARG_NUM}" -eq 0 ]]; then
    err=1;
		echo "cd_ expects a single argument."
	# function called with 1 or more args
	else
    # regex to test for any ls command line parameters, i.e. -alh, OR --help
    OPTIONS_REGEX="(^\-|^\--)\w+$"
    # when one or more args exist, iterate them
    for c in "${!MAP[@]}"; do
      # if there is a period in the text, expand pwd into CWD		
		  if [ "$c" = "." ]; then
        CWD="${PWD}"
			# check to see if ls command options -/-- were passed in error
      elif [[ "${c}" =~ $OPTIONS_REGEX ]]; then
				err=1;
				echo "cd_ does not accept arguments!"
			else
				# only other valid options passed could be path, so set CWD to path
				CWD="${c}"
      fi
    done
  fi
	# if there are no errors, continue
	if [[ $err -eq 0 ]]; then
		# if the directory exists proceed
		if [ -d "${CWD}" ]; then
			# save the output to the history file
  		(echo "${CWD}" >> $CD_HIST_FILE)
  		# finally execute command if error free
  		cd $CWD
		else
			echo "${CWD} is not a valid path."
		fi
	fi
}
# ls_hist: A history of the user's ls_ command usage
# This function will display a list of numbered paths including the command options
# origionally supplied in order of a user's command execution history. 
# The user will be prompted to enter an integer number representing the path history.
# The user's corresponding chosen path will then be listed / enumerated.
function ls_hist() {
	# load the array from history file and display
	declare -i iter=0
	declare -i real_choice=0
	mapfile -t cmd_hist < ${LS_HIST_FILE}
	arr_len=${#cmd_hist[@]}
	for (( i = 0 ; i < arr_len ; i++ ))
		do
			iter=$(($i+1))
  		echo "[$iter]: ${cmd_hist[$i]}"
	done
	echo "Enter your numeric choice or q to quit..."
	# account for the array offset after reading selection
	read selection
	if [ "${selection}" != "q" ]; then
		real_choice=$((${selection}-1))
		ls_ ${cmd_hist[${real_choice}]}
	fi
}
# cd_hist: A history of the user's cd_ command usage
# This function will display a list of numbered paths in order of a user's command
# execution history. 
# The user will be prompted to enter an integer number representing the path history.
# The user's path will then be cd'd to the corresponding path.
function cd_hist() {
	# load the array from history file and display
	declare -i iter=0
	declare -i real_choice=0
	mapfile -t cmd_hist < ${CD_HIST_FILE}
	arr_len=${#cmd_hist[@]}
	for (( i = 0 ; i < arr_len ; i++ ))
		do
			iter=$(($i+1))
  		echo "[$iter]: ${cmd_hist[$i]}"
	done
	echo "Enter your numeric choice or q to quit..."
	# account for the array offset after reading selection
	read selection
	if [ "${selection}" != "q" ]; then
		real_choice=$((${selection}-1))
		ls_ ${cmd_hist[${real_choice}]}
	fi
}
